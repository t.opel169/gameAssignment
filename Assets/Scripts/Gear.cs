﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum GearName
{
    First = 1,
    Second = 2,
    Third = 3,
    Fourth = 4,
    Fifth = 5
}

public class Gear
{
    public GearName GearName { get; set; }
    public float GearStartSpeed { get; set; }
    public float GearMaxSpeed { get; set; }
    public float NormalAcceleration { get; set; }
    public float LowAcceleration { get; set; }
    public int GearNumber
    {
        get
        {
            return (int)GearName;
        }
    }
    public Gear PreviousGear { get; set; }
    public Gear NextGear { get; set; }

    public Gear(GearName gearName, float gearStartSpeed, float gearMaxSpeed, Gear previousGear)
    {
        GearName = gearName;
        GearStartSpeed = gearStartSpeed;
        GearMaxSpeed = gearMaxSpeed;
        PreviousGear = previousGear;
    }

    // How much the game should accelerate with the current gear
    public float GetCurrentAcceleration(float currentSpeed)
    {
        if (currentSpeed <= GearStartSpeed + 3 || currentSpeed >= GearMaxSpeed - 3)
        {
            return 0.6f;
        }
        else if (currentSpeed >= GearMaxSpeed)
        {
            return 0.0f;
        }
        else
        {
            return 1;
        }
    }

    // Used for indicating which gear change circle should appear
    public int GetAccelerationRangeIndex(float currentSpeed)
    {
        if (currentSpeed <= GearStartSpeed + 3)
        {
            return 0;
        }
        else if (currentSpeed >= GearMaxSpeed - 3)
        {
            return 2;
        }
        else if (currentSpeed >= GearMaxSpeed - 5)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}
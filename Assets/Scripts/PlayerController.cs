﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float InitialSpeed;
    public bool IsOponnent = false;
    private float CurrentSpeed;
    private GameController GameController;
    private Rigidbody PlayerRigidBody;
    private Color InitialColor;
    private bool IsInvincible;
    private Renderer PlayerRenderer;

    private void Start()
    {
        GameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        // Difficulty in non-generated influences turning speed
        CurrentSpeed = !GameController.GenerateLevel && PlayerSettings.IsHardDifficulty ? InitialSpeed - 5 : InitialSpeed;
        PlayerRenderer = GetComponent<Renderer>();
        InitialColor = PlayerRenderer.material.color;
        PlayerRigidBody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (!GameController.GameEnded)
        {
            if (!IsOponnent)
            {
                PlayerMovement(Input.GetAxis("Horizontal"));
            }
            //Jump();
        }
        else
        {
            PlayerRigidBody.velocity = new Vector3();
        }
    }

    // Method for currently unused powerup
    private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            var jumpVector = new Vector3(0, 3, 0);
            PlayerRigidBody.AddForce(Vector3.up * 1000);
            //transform.Translate(jumpVector, Space.World);
        }
    }

    public void PlayerMovement(float xMovement, float zMovement = 0.0f)
    {
        if (GameController.GameStarted)
        {
            var movement = new Vector3(xMovement, 0.0f, zMovement);
            var movementVelocity = movement * CurrentSpeed;
            // If this is a fixed level, we need to move the player forward as well
            if (!GameController.GenerateLevel)
            {
                movementVelocity += GameController.GameSpeed * Vector3.forward;
            }
            PlayerRigidBody.velocity = movementVelocity;

            PlayerRigidBody.position = new Vector3
            (
                Mathf.Clamp(PlayerRigidBody.position.x, GameController.XMin, GameController.XMax),
                PlayerRigidBody.position.y,
                PlayerRigidBody.position.z
            );
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Boundary")
        {
            if (other.gameObject.tag == "Obstacle" && !IsInvincible && !IsOponnent)
            {
                GameController.EndGame(false);
                PlayerRigidBody.velocity = new Vector3();
                //Destroy(gameObject);
            }
            else if (other.gameObject.tag == "Finish")
            {
                GameController.EndGame(true);
            }
            else if (other.gameObject.tag == "SpeedBooster")
            {
                GameController.IncreaseSpeed(3);
            }
            else if (other.gameObject.tag == "SteerBooster")
            {
                // I have not found another way to play 2D sound on object destroy, play at point plays 3D sound
                GameObject.FindGameObjectWithTag("PowerUpSound").GetComponent<AudioSource>().Play();
                Destroy(other.gameObject);
                CurrentSpeed = InitialSpeed * 2;
                PlayerRenderer.material.color = other.gameObject.GetComponent<Renderer>().material.color;
                Invoke("ResetSteerSpeed", 2.5f);
            }
            else if (other.gameObject.tag == "SteerDowngrade")
            {
                GameObject.FindGameObjectWithTag("PowerDownSound").GetComponent<AudioSource>().Play();
                Destroy(other.gameObject);
                CurrentSpeed = InitialSpeed / 2;
                PlayerRenderer.material.color = other.gameObject.GetComponent<Renderer>().material.color;
                Invoke("ResetSteerSpeed", 2.5f);
            }
            else if (other.gameObject.tag == "InvinciblePowerUp")
            {
                GameObject.FindGameObjectWithTag("PowerUpSound").GetComponent<AudioSource>().Play();
                IsInvincible = true;
                PlayerRenderer.material.color = other.gameObject.GetComponent<Renderer>().material.color;
                Destroy(other.gameObject);
                Invoke("ResetInvincible", 3f);
            }
            // not used currently
            //else if (other.gameObject.tag == "JumpPowerUp")
            //{

            //}
        }
    }

    private void ResetSteerSpeed()
    {
        CurrentSpeed = InitialSpeed;
        PlayerRenderer.material.color = InitialColor;
    }

    private void ResetInvincible()
    {
        IsInvincible = false;
        PlayerRenderer.material.color = InitialColor;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    // This attribute changes behavior of this class
    public bool GenerateLevel;
    public Gear CurrentGear;
    public Text GearText;
    public Text SpeedText;
    public Text TimeText;
    public float XMin;
    public float XMax;
    public GameObject PauseMenu;
    public List<GameObject> GearImages;

    // These parameters are only used for generated levels
    public GameObject Obstacle;
    public GameObject SpeedBooster;
    public GameObject SteerBooster;
    public GameObject SteerDowngrade;
    public GameObject JumpPowerUp;
    public GameObject InvinciblePowerUp;
    public GameObject FinishLine;
    public Vector3 ObstaclePositionLeft;
    public Vector3 ObstaclePositionMiddle;
    public Vector3 ObstaclePositionRight;
    public float ObstacleSpawnWait;    
    // These parameters influence difficulty
    public float DistanceBetweenObstacles = 20f;
    public float LevelTimeLimit;
    public int ObstaclesToPass;

    public List<Gear> AvailableGears { get; set; }
    public bool GameEnded { get; set; }
    public float GameSpeed { get; set; }
    public bool GameStarted { get; set; }
    private List<Vector3> SpawnPositions;
    private IEnumerator ObstaclesCoroutine;
    private IEnumerator BoostersCoroutine;
    private LevelManager LevelManager;
    private GameObject LastObstacle;
    private GameObject LastBooster;
    private float AccelerationTime = 0.2f;
    private float CurrentAccelerationTime = 0f;
    private bool FinishLineCreated;
    private float TimeElapsed;

    private void Start()
    {
        // Wait for camera animation to complete
        Invoke("StartGame", 4);
    }

    private void Update()
    {
        if (GameStarted)
        {
            if (GenerateLevel)
            {
                LevelTimeLimit -= Time.deltaTime;

                if (LevelTimeLimit <= 0)
                {
                    EndGame(false);
                }

                if (ObstaclesToPass <= 0 && !FinishLineCreated)
                {
                    StopCoroutine(ObstaclesCoroutine);
                    StopCoroutine(BoostersCoroutine);
                    FinishLineCreated = true;
                    Invoke("CreateFinishLine", 1.5f);
                }
            }

            if (!GameEnded)
            {
                TimeElapsed += Time.deltaTime;

                SetTimeText();
                IncreaseAcceleration();
                SwitchGears();

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    PauseGame();
                }
            }
        }
    }

    private void StartGame()
    {
        GameSpeed = 1;
        if (GenerateLevel)
        {
            SpawnPositions = new List<Vector3>
            {
                ObstaclePositionLeft,
                ObstaclePositionMiddle,
                ObstaclePositionRight
            };
            ObstaclesCoroutine = GenerateObstacles();
            StartCoroutine(ObstaclesCoroutine);
            BoostersCoroutine = GenerateBoosters();
            StartCoroutine(BoostersCoroutine);

            if (PlayerSettings.IsHardDifficulty)
            {
                LevelTimeLimit -= 10;
            }
        }

        CreateGears();
        LevelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
        GameStarted = true;
    }

    private void CreateFinishLine()
    {
        Instantiate(FinishLine, SpawnPositions[1], Quaternion.identity);
    }

    private void SetTimeText()
    {
        if (GenerateLevel)
        {
            TimeText.text = string.Format("Remaining time: {0}", LevelTimeLimit);
        }
        else
        {
            TimeText.text = string.Format("Time: {0}", TimeElapsed);
        }
    }

    public void EndGame(bool victory)
    {
        TimeText.text = "0";
        GameSpeed = 0;
        GameEnded = true;
        LevelManager.LoadScene(victory ? "Win" : "Lose");
        if (victory)
        {
            switch (SceneManager.GetActiveScene().name)
            {
                case "Level_01":
                    PlayerSettings.Level1Time = PlayerSettings.Level1Time > TimeElapsed ? TimeElapsed : PlayerSettings.Level1Time;
                    break;
                case "Level_02":
                    PlayerSettings.Level2Time = PlayerSettings.Level2Time > TimeElapsed ? TimeElapsed : PlayerSettings.Level2Time;
                    break;
            }
        }
    }

    private void CreateGears()
    {
        var firstGear = new Gear(GearName.First, 0, 15, null);
        var secondGear = new Gear(GearName.Second, 12, 27, firstGear);
        firstGear.NextGear = secondGear;
        var thirdGear = new Gear(GearName.Third, 24, 39, secondGear);
        secondGear.NextGear = thirdGear;
        var fourthGear = new Gear(GearName.Fourth, 36, 51, thirdGear);
        thirdGear.NextGear = fourthGear;
        //var fifthGear = new Gear(GearName.Fifth, 48, 63, fourthGear);
        AvailableGears = new List<Gear>
        {
            firstGear,
            secondGear,
            thirdGear,
            fourthGear,
            //fifthGear
        };
        CurrentGear = firstGear;
    }

    public void IncreaseSpeed(float increase)
    {
        if (GameSpeed != CurrentGear.GearMaxSpeed)
        {
            var increasedSpeed = GameSpeed + increase;
            GameSpeed = increasedSpeed > CurrentGear.GearMaxSpeed ? GameSpeed : increasedSpeed;
            SpeedText.text = "Speed: " + GameSpeed.ToString("0.00");
        }
    }

    private void IncreaseAcceleration()
    {
        if (CurrentAccelerationTime >= AccelerationTime)
        {
            CurrentAccelerationTime = 0f;
            IncreaseSpeed(CurrentGear.GetCurrentAcceleration(GameSpeed));
            ShowAccelerationImage();
        }
        else
        {
            CurrentAccelerationTime += Time.deltaTime;
        }
    }

    private void ShowAccelerationImage()
    {
        var listIndex = CurrentGear.GetAccelerationRangeIndex(GameSpeed);
        for (var i = 0; i <= listIndex; i++)
        {
            GearImages[i].SetActive(true);
        }
    }

    private void HideAccelerationImages()
    {
        GearImages[1].SetActive(false);
        GearImages[2].SetActive(false);
    }

    private void SwitchGears()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            SetNextGear();
        }
    }

    public void SetNextGear()
    {
        if (CurrentGear.NextGear != null)
        {
            GameObject.FindGameObjectWithTag("GearChangeSound").GetComponent<AudioSource>().Play();
            CurrentGear = CurrentGear.NextGear;
            GearText.text = string.Format("Current Gear: {0}/{1}", CurrentGear.GearNumber, AvailableGears.Count);
            HideAccelerationImages();
        }
    }

    private IEnumerator GenerateObstacles()
    {
        while (!GameEnded)
        {
            Vector3 spawnPosition;
            if (LastObstacle == null)
            {
                // First location selected randomly
                spawnPosition = SpawnPositions[UnityEngine.Random.Range(0, SpawnPositions.Count)];
            }
            else
            {
                // Next location must not be in the same lane
                spawnPosition = SpawnPositions.Where(position => position.x != LastObstacle.transform.position.x).ToList()[UnityEngine.Random.Range(0, SpawnPositions.Count - 1)];
            }
            LastObstacle = Instantiate(Obstacle, spawnPosition + Obstacle.transform.position, Quaternion.identity);
            // Wait until there is enough distance between the previous and new obstacle
            while (Vector3.Distance(LastObstacle.transform.position, SpawnPositions[1]) < DistanceBetweenObstacles)
            {
                yield return new WaitForSeconds(0.5f);
            }
            ObstaclesToPass--;
        }
    }

    private IEnumerator GenerateBoosters()
    {
        while (!GameEnded)
        {
            Vector3 spawnPosition;
            if (LastBooster == null)
            {
                var firstBoosterPosition = SpawnPositions[UnityEngine.Random.Range(0, SpawnPositions.Count)];
                firstBoosterPosition.z -= 50;
                spawnPosition = firstBoosterPosition;
            }
            else
            {
                spawnPosition = SpawnPositions.Where(position => position.x != LastBooster.transform.position.x).ToList()[UnityEngine.Random.Range(0, SpawnPositions.Count - 1)];
            }
            var randomBooster = ChooseBoosterRandomly();
            spawnPosition += randomBooster.transform.position; 
            // Only instantiate if the booster will not intersect with the obstacle
            if (Vector3.Distance(LastObstacle.transform.position, spawnPosition) > 7)
            {
                LastBooster = Instantiate(randomBooster, spawnPosition, Quaternion.identity);
            }
            
            while (Vector3.Distance(LastBooster.transform.position, SpawnPositions[1]) < 40)
            {
                yield return new WaitForSeconds(0.5f);
            }
        }
    }

    // Randomly choose booster, each one has a different weight
    private GameObject ChooseBoosterRandomly()
    {
        var randomNumber = UnityEngine.Random.Range(0, 6);
        if (randomNumber <= 2)
        {
            return SpeedBooster;
        }
        else if (randomNumber == 3)
        {
            return SteerBooster;
        }
        else if (randomNumber == 4)
        {
            return SteerDowngrade;
        }
        else
        {
            return InvinciblePowerUp;
        }
    }

    public void PauseGame()
    {
        if (PauseMenu.activeInHierarchy)
        {
            PauseMenu.SetActive(false);
            Time.timeScale = 1;
        }
        else
        {
            PauseMenu.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
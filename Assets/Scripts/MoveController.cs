﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    public int Direction = -1;
    private float Speed;
    private GameController GameController;

    private void Start()
    {
        GameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    private void Update()
    {
        Speed = Direction * GameController.GameSpeed;
        var rigidBody = GetComponent<Rigidbody>();
        rigidBody.velocity = Vector3.forward * Speed;
    }
}

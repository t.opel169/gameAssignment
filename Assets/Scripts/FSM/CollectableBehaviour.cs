﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class CollectableBehaviour : MonoBehaviour
{
    public FSMSystem Fsm;
    public float Range = 25f;
    public bool SeekBehaviour = true;
    public float TurningSpeed;
    private float Speed;
    private GameObject Player;
    private Rigidbody CollectableRigidbody;
    private GameController GameController;

    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        CollectableRigidbody = GetComponent<Rigidbody>();
        GameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        MakeFSM();
    }

    private void MakeFSM()
    {
        var idleState = new CollectableIdleState();
        var seekState = new CollectableSeekState(this);
        var fleeState = new CollectableFleeState(this);

        idleState.AddTransition(Transition.IsNearPlayerFlee, StateID.FleeingPlayerID);
        idleState.AddTransition(Transition.IsNearPlayerSeek, StateID.SeekingPlayerID);
        seekState.AddTransition(Transition.IsFarFromPlayerSeek, StateID.IdleID);
        fleeState.AddTransition(Transition.IsFarFromPlayerFlee, StateID.IdleID);

        Fsm = new FSMSystem();
        Fsm.AddState(idleState);
        Fsm.AddState(seekState);
        Fsm.AddState(fleeState);
    }

    public void SetTransition(Transition t)
    {
        Fsm.PerformTransition(t);
    }

    void Update()
    {
        Speed = GameController.GameSpeed;
        Fsm.CurrentState.Reason(gameObject, Player);
        Fsm.CurrentState.Act(gameObject, Player);
    }

    public bool IsPlayerInRange(GameObject collectable, GameObject player)
    {
        var distance = Vector3.Distance(collectable.transform.position, player.transform.position);
        var ray = new Ray(collectable.transform.position, (player.transform.position - collectable.transform.position).normalized);
        var playerRenderer = player.GetComponent<Renderer>();
        if (distance < Range && playerRenderer.bounds.IntersectRay(ray))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void SeekFlee(GameObject collectable, GameObject player)
    {
        var desiredVelocity = SeekBehaviour ? (player.transform.position - collectable.transform.position)
            : (collectable.transform.position - player.transform.position);

        desiredVelocity.Normalize();
        desiredVelocity *= Speed;

        var currentVelocity = CollectableRigidbody.velocity;
        var steering = desiredVelocity - currentVelocity;

        //limit the max steering so that the player can interact, otherwise he would could not influence the result
        if (steering.sqrMagnitude > TurningSpeed * TurningSpeed)
        {
            steering.Normalize();
            steering *= TurningSpeed;
        }

        currentVelocity = currentVelocity + steering / CollectableRigidbody.mass;
        currentVelocity.Normalize();
        currentVelocity *= Speed;

        CollectableRigidbody.velocity = currentVelocity;

        //clamp so that the object does not go off the plane when fleeing
        CollectableRigidbody.position = new Vector3
        (
            Mathf.Clamp(CollectableRigidbody.position.x, GameController.XMin, GameController.XMax),
            player.transform.position.y,
            CollectableRigidbody.position.z
        );
    }
}

public class CollectableIdleState : FSMState
{
    public CollectableIdleState()
    {
        stateID = StateID.IdleID;
    }

    public override void Act(GameObject collectable, GameObject player)
    {
      
    }

    public override void Reason(GameObject collectable, GameObject player)
    {
        var collectableBehaviour = collectable.GetComponent<CollectableBehaviour>();
        if (collectableBehaviour.IsPlayerInRange(collectable, player))
        {
            if (collectableBehaviour.SeekBehaviour)
            {
                collectableBehaviour.SetTransition(Transition.IsNearPlayerSeek);
            }
            else
            {
                collectableBehaviour.SetTransition(Transition.IsNearPlayerFlee);
            }
        }
    }
}

public class CollectableFleeState : FSMState
{
    private CollectableBehaviour CollectableBehaviour;

    public CollectableFleeState(CollectableBehaviour collectableBehaviour)
    {
        stateID = StateID.FleeingPlayerID;
        CollectableBehaviour = collectableBehaviour;
    }

    public override void Act(GameObject collectable, GameObject player)
    {
        CollectableBehaviour.SeekFlee(collectable, player);
    }

    public override void Reason(GameObject collectable, GameObject player)
    {
        var collectableBehaviour = collectable.GetComponent<CollectableBehaviour>();
        if (!collectableBehaviour.IsPlayerInRange(collectable, player))
        {
            collectableBehaviour.SetTransition(Transition.IsFarFromPlayerFlee);
        }
    }
}

public class CollectableSeekState : FSMState
{
    private CollectableBehaviour CollectableBehaviour;

    public CollectableSeekState(CollectableBehaviour collectableBehaviour)
    {
        stateID = StateID.SeekingPlayerID;
        CollectableBehaviour = collectableBehaviour;
    }

    public override void Act(GameObject collectable, GameObject player)
    {
        CollectableBehaviour.SeekFlee(collectable, player);
    }

    public override void Reason(GameObject collectable, GameObject player)
    {
        if (!CollectableBehaviour.IsPlayerInRange(collectable, player))
        {
            CollectableBehaviour.SetTransition(Transition.IsFarFromPlayerSeek);
        }
    }
}
﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class OpponentBehaviour : MonoBehaviour
{
    public FSMSystem fsm;
    public float obstacleSight = 10;

    void Start()
    {
        MakeFSM();
    }

    private void MakeFSM()
    {
        OpponentIdleState idle = new OpponentIdleState();
        EvadingState evade = new EvadingState();

        idle.AddTransition(Transition.SawObstacle, StateID.EvadingID);
        evade.AddTransition(Transition.LostObstacle, StateID.IdleID);

        fsm = new FSMSystem();
        fsm.AddState(idle);
        fsm.AddState(evade);
    }

    public void SetTransition(Transition t)
    {
        fsm.PerformTransition(t);
    }

    void Update()
    {
        var obstacles = GameObject.FindGameObjectsWithTag("Obstacle");
        var closestObject = GetClosestObject(gameObject, obstacles);
        fsm.CurrentState.Reason(gameObject, closestObject);
        fsm.CurrentState.Act(gameObject, closestObject);
    }

    public GameObject GetClosestObject(GameObject player, GameObject[] objects)
    {
        GameObject closestObject = null;
        var objectsFrontPlayer = objects.Where(o => o.transform.position.z >= player.transform.position.z);
        if (objectsFrontPlayer.Count() != 0)
        {
            closestObject = objectsFrontPlayer.OrderBy(o => o.transform.position.z).First();
        }
        return closestObject;
    }

    public bool ObjectOnCollision(GameObject player, GameObject closestObject)
    {
        if (closestObject != null)
        {
            var objectRenderer = closestObject.GetComponent<Renderer>();
            var ray = new Ray(player.transform.position, new Vector3(0.0f, 0.0f, 1.0f));
            if (objectRenderer.bounds.IntersectRay(ray) && (closestObject.transform.position.z - player.transform.position.z < obstacleSight))
            {
                Debug.Log("Intersection");
                return true;
            }
            else
            {
                Debug.Log("No intersection");
                return false;
            }
        }
        return false;
    }
}


public class OpponentIdleState : FSMState
{
    public OpponentIdleState()
    {
        stateID = StateID.IdleID;
    }

    public override void Act(GameObject player, GameObject closestObject)
    {
        var playerController = player.GetComponent<PlayerController>();
        // Randomly slow down or speed up, this makes the opponent stay near the target
        playerController.PlayerMovement(0, Random.Range(-0.02f, 0.01f));
    }

    public override void Reason(GameObject player, GameObject closestObject)
    {
        var opponentBehaviour = player.GetComponent<OpponentBehaviour>();
        if (player.GetComponent<OpponentBehaviour>().ObjectOnCollision(player, closestObject))
        {
            opponentBehaviour.SetTransition(Transition.SawObstacle);
        }
    }
}

public class EvadingState : FSMState
{
    private GameObject ObjectToEvade;
    private float RemainingRadiusX;
    private float CenterX;

    public EvadingState()
    {
        stateID = StateID.EvadingID;
    }

    public override void Act(GameObject player, GameObject closestObject)
    {
        var playerController = player.GetComponent<PlayerController>();
        if (RemainingRadiusX > 0)
        {
            // This is the center position in Level 2
            if (CenterX <= 186.86)
            {
                playerController.PlayerMovement(1f);
            }
            else
            {
                playerController.PlayerMovement(-1f);
            }
            RemainingRadiusX--;
        }
    }

    public override void Reason(GameObject player, GameObject closestObject)
    {
        var opponentBehaviour = player.GetComponent<OpponentBehaviour>();
        ObjectToEvade = opponentBehaviour.ObjectOnCollision(player, closestObject) ? closestObject : null;
        if (ObjectToEvade == null)
        {
            if (RemainingRadiusX <= 0)
            {
                opponentBehaviour.SetTransition(Transition.LostObstacle);
            }
        }
        else
        {
            var objectRenderer = ObjectToEvade.GetComponent<Renderer>();
            RemainingRadiusX = objectRenderer.bounds.extents.x + 6;
            CenterX = objectRenderer.bounds.center.x;
        }
    }
}

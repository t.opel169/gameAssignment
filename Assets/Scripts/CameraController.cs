﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject Player;
    private Vector3 Offset;
    private GameController GameController;

    private void Start()
    {
        Offset = transform.position;
        GameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    private void LateUpdate()
    {
        if (Player != null && GameController.GameStarted)
        {
            var newCameraPosition = Player.transform.position + Offset;
            newCameraPosition.y = 6;
            transform.position = newCameraPosition;
        }
    }
}
